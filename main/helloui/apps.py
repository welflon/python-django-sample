from django.apps import AppConfig


class HellouiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'helloui'
