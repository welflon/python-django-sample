from django import forms


def validate_only_letters(value):
    if not value.isalpha():
        raise forms.ValidationError('Imie musi składać sie tylko z liter!')


class CheckForm(forms.Form):
    check_name = forms.CharField(max_length=30, min_length=3, validators=[validate_only_letters])


class AddForm(forms.Form):
    add_name = forms.CharField(max_length=30, min_length=3, validators=[validate_only_letters])
