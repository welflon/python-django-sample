# Generated by Django 4.1.5 on 2024-06-18 08:01

from django.db import migrations, models
import helloworldapp.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Names',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=30, unique=True)),
                ('sex', models.CharField(choices=[(helloworldapp.models.SexEnum['MALE'], helloworldapp.models.SexEnum['FEMALE']), (helloworldapp.models.SexEnum['MALE'], helloworldapp.models.SexEnum['FEMALE'])], max_length=10)),
            ],
        ),
    ]
