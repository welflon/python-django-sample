from datetime import datetime
from django.shortcuts import render
from .utils import get_or
from .models import Names
from .forms import CheckForm
from .forms import AddForm


# names = {"ADRIAN", "ANIA", "ALDONA", "ADAM"}


def homepage(request):
    if request.method == 'POST':
        data = request.POST
        if 'check_name' in data:
            return check_name_form(request)
        elif 'add_name' in data:
            return add_name_form(request)
        else:
            return render(request, 'helloworldapp/homepage.html')
    else:
        return render(request, 'helloworldapp/homepage.html')


def check_name_form(request):
    value = str(request.POST.get('check_name'))
    check_form = CheckForm(data=request.POST)
    result_valid = is_correct_name(check_form)
    names = Names.objects.all()
    if len(result_valid) > 0:
        return render(request, 'helloworldapp/homepage.html', context={'errors': result_valid, 'names': names})
    else:

        value = str(value).upper()
        is_exist = get_or(Names, {'name': value})
        result = build_text_for_homepage(is_exist, value)
        return render(request, 'helloworldapp/homepage.html', context={'form_content': result, 'names': names})


def add_name_form(request):
    value = str(request.POST.get('add_name'))
    add_form = AddForm(data=request.POST)
    result_valid = is_correct_name(add_form)
    names = Names.objects.all()
    if len(result_valid) > 0:
        return render(request, 'helloworldapp/homepage.html',
                      context={'errors': result_valid, 'add_content': "", 'names': names})
    else:
        value = str(value).upper()
        name_entity, created = Names.objects.get_or_create(name=value)
        if created:
            name_entity.sex = recognise_sex(name_entity.name)
            name_entity.save()
            names = Names.objects.all()
            add_content = f'Imie zostało dodane poprawnie. A płeć określono na {name_entity.sex}'
            return render(request, 'helloworldapp/homepage.html', context={'add_content': add_content, 'names':names})
        else:
            result_valid.append("Imię juz znajduje sie w zbiorze.")
            return render(request, 'helloworldapp/homepage.html',
                          context={'errors': result_valid, 'add_content': "", 'names': names})


def is_correct_name(form):
    errors = []
    if not form.is_valid():
        for field, issues in form.errors.items():
            for error in issues:
                errors.append(error)
    return errors


def build_text_for_homepage(is_exist, value):
    nie = " nie"
    content = " zawiera sie w zbiorze imion"
    if is_exist:
        return value + content
    else:
        return value + nie + content


def otherpage(request):
    time = datetime.now()
    return render(request, 'helloworldapp/otherpage.html', context={'time': time})


def recognise_sex(name):
    if name[-1] == "A":
        return "FEMALE"
    else:
        return "MALE"
