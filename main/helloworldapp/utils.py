def get_or(model, search, to_return=None):
    try:
        return model.objects.get(**search)
    except model.DoesNotExist:
        return to_return
