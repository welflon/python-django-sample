from enum import Enum

from django.db import models


class SexEnum(Enum):
    MALE = 'MALE',
    FEMALE = 'FEMALE'


class Names(models.Model):
    id = models.AutoField(primary_key=True) #PK jest dodawany z automatu nie trzeba go tutaj wypisywać
    name = models.CharField(max_length=30, unique=True)
    sex = models.CharField(choices=[(sex.MALE, sex.FEMALE) for sex in SexEnum], max_length=10)

    def __str__(self):
        return f'{self.name}:{self.sex}'

