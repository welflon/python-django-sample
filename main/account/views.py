from django.contrib import auth
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect

from .forms import SignupForm, LoginForm


def signup(request):
    if request.method == 'POST':
        errors = check_validation(SignupForm(data=request.POST))
        if len(errors) > 0:
            return render(request, 'account/signup.html', context={'errors': errors})
        else:
            try:
                User.objects.get(username=request.POST['username'])
                errors.append("User already exists")
                return render(request, 'account/signup.html', context={'errors': errors})
            except User.DoesNotExist:
                username = request.POST['username']
                email = request.POST['email']
                password = request.POST['password']
                user = User.objects.create_user(username=username, email=email, password=password)
                user.save()
                auth.login(request, user)
                # success_message = "User created successfully!"  TODO add green information on main page
                return redirect('helloui:homepage')
    else:
        return render(request, 'account/signup.html')


def check_validation(form):
    errors = []
    if not form.is_valid():
        for field, issues in form.errors.items():
            for error in issues:
                errors.append(error)

    return errors


def login(request):
    if request.method == 'POST':
        errors = check_validation(LoginForm(data=request.POST))
        if len(errors) > 0:
            return render(request, 'account/login.html', context={'errors': errors})
        else:
            username_or_email = request.POST['username_or_email']
            password = request.POST['password']

            if '@' in username_or_email:
                try:
                    username = User.objects.get(email=username_or_email).username
                    return login_authenticate(request, username, password)
                except ObjectDoesNotExist:
                    return login_filed(request)
            else:
                try:
                    username = User.objects.get(username=username_or_email).username
                    return login_authenticate(request, username, password)
                except ObjectDoesNotExist:
                    return login_filed(request)
    else:
        return render(request, 'account/login.html')


def login_authenticate(request, username, password):
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request, user)
        return redirect('helloui:homepage')
    else:
        return login_filed(request)


def login_filed(request):
    errors = ["Login failure, Please contact with administrator"]
    return render(request, 'account/login.html', context={'errors': errors})


def logout(request):
    if request.method == 'POST':
        auth.logout(request)

    return render(request, 'account/logout.html')
