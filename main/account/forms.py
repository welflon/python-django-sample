from django import forms


class SignupForm(forms.Form):
    username = forms.CharField(max_length=40, min_length=3, required=True)
    confirm_password = forms.CharField(widget=forms.PasswordInput, min_length=3, required=True)
    password = forms.CharField(widget=forms.PasswordInput, min_length=3, required=True)
    email = forms.EmailField(max_length=40)

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password and confirm_password and password != confirm_password:
            self.add_error('confirm_password', "Passwords don't match")

        return cleaned_data


class LoginForm(forms.Form):
    username_or_email = forms.CharField(max_length=30, min_length=3, required=True)
    password = forms.CharField(widget=forms.PasswordInput, min_length=3, required=True)